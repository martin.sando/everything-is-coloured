import tkinter as tk
from tkinter import filedialog
import numpy as np
import cv2
from PIL import Image, ImageTk

def open_img():
    global img_str
    img_str = filedialog.askopenfilename()

def take():
    global root2, scale_blur, scale_colours, K, N
    K = scale_colours.get()
    N = scale_blur.get()
    root2.destroy()

def setup():
    global scale_blur, scale_colours, root2
    
    root2 = tk.Tk()
    root2.title('Setup')
    root2 ['bg'] = '#f5e2bc'
    root2.geometry('400x250')

    settings = tk.Label(root2, text = 'Settings of colouring', bg = '#f5e2bc', fg = 'red', font = 'Arial 20')
    settings.grid(row = 1, column = 0)
    
    frame2 = tk.Frame(root2, bg = '#f5e2bc')
    
    number_of_colours = tk.Label(frame2, text = 'Choose number of colours:', bg = '#f5e2bc', font = 'Arial 14')
    number_of_colours.grid(row = 0, column = 0)
    
    scale_colours = tk.Scale(frame2, orient = 'horizontal', bg = '#f5e2bc', troughcolor = '#fcae18', highlightbackground = '#f5e2bc', font = 'Arial 14', length = 150, from_ = 2, to = 10, tickinterval = 2, resolution = 1)  
    scale_colours.grid(row = 0, column = 1)

    amount_of_blur = tk.Label(frame2, text = 'Choose the amount of blur:', bg = '#f5e2bc', font = 'Arial 14')
    amount_of_blur.grid(row = 1, column = 0)

    scale_blur = tk.Scale(frame2, orient = 'horizontal', bg = '#f5e2bc', troughcolor = '#fcae18', highlightbackground = '#f5e2bc', font = 'Arial 14', length = 150, from_ = 1, to = 5, tickinterval = 1, resolution = 1)  
    scale_blur.grid(row = 1, column = 1)
    
    frame2.grid(row = 2, column = 0)

    b_ok = tk.Button(root2, text = 'OK', bg = '#fcae18', font = 'Arial 18', width = 5, height = 0, command = take)
    b_ok.grid(row = 3, column = 0)
    
    root2.mainloop()
    
def main():
    global img_str, K, N
    
    stream = open(img_str, 'rb')
    bytes = bytearray(stream.read())
    numpyarray = np.asarray(bytes, dtype=np.uint8)
    img = cv2.imdecode(numpyarray, cv2.IMREAD_UNCHANGED)
    
    height, weight = img.shape[:2]
    just_cont = np.zeros((np.size(img, 0), np.size(img, 1), 3), np.uint8)
    just_cont.fill(255)

    blur = [9, 15, 25, 51, 101]
    T = blur[N-1]
    img = cv2.GaussianBlur (img, (T, T), 0)

    Z = img.reshape((-1,3))
    # convert to np.float32
    Z = np.float32(Z)

    # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    ret, label, center=cv2.kmeans(Z, K, None, criteria, 3, cv2.KMEANS_RANDOM_CENTERS)
    # Now convert back into uint8, and make original image
    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img.shape))
    res3 = res2.copy()
    
    print(center)
    all_contours = []

    def Del_Small_Cont(t):
        if len(t)<30:
            #col = img[t[0][0]]
            #cv2.fillPoly(res2, t[0],  tuple([int(x) for x in col]))
            return 0
        else:
            return 1

    for i in range(K-1):
        colour = center[i]
        thresh = cv2.inRange(res2, colour, colour) # применяем цветовой фильтр
        # ищем контуры и складируем их в переменную contours
        res2[thresh>0] = center[i+1]
        contours = cv2.findContours( thresh, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)[0]
        Just_Big_Cont = list(filter(Del_Small_Cont, contours))
        cv2.drawContours( just_cont, Just_Big_Cont, -1, (0,0,0), 1, cv2.LINE_AA)
        all_contours.append(Just_Big_Cont)
       
    just_cont = cv2.rectangle(just_cont, (1, 1), (weight-1, height-1), (0,0,0), 2) #рисуем рамку вокруг раскраски, чтобы заливка не просачивалась через её края
    cv2.imshow('colouring', just_cont)
    cv2.imshow('result', res3)
    cv2.imwrite("cont.jpg", just_cont)
    cv2.imwrite("res2.jpg", res3)

    def click(event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDBLCLK:
            color = res3[y, x]
            print((color[0],color[1],color[2]))
            fill(x, y, color)
            
    def fill(x, y, color):
        h, w = just_cont.shape[:2]
        mask = np.zeros((h+2, w+2), np.uint8)
        mask[:]=0
        cv2.floodFill(just_cont, mask, (x, y), (int(color[0]),int(color[1]),int(color[2])))
        cv2.imshow('colouring', just_cont)

    cv2.setMouseCallback('colouring',click)
    cv2.waitKey(0) 
    cv2.destroyAllWindows()

root = tk.Tk()
root.title('Everything is coloured')
root ['bg'] = '#f5e2bc'
root.geometry('770x500')

welcome = tk.Label(root, text = 'Welcome to "Everything is coloured!"', fg = 'red', font = 'Georgia 32', bg = '#f5e2bc', padx = 25, pady = 20)
welcome.grid(row = 1, column = 0)

order = tk.Label(root, text = 'Order of actions (Just 3 steps!)', fg = 'red', font = 'Georgia 24', bg = '#f5e2bc')
order.grid(row = 2, column = 0)

frame1 = tk.Frame(root, bg = '#f5e2bc', pady = 20)

step_one = tk.Label(frame1, text = 'Step 1:', font = 'Georgia 14', bg = '#f5e2bc')
step_one.grid(row = 1, column = 0)

b_open = tk.Button(frame1, text = 'Open image', bg='#fcae18', font = 'Arial 16', command = open_img)
b_open.grid(row = 1, column = 1)

step_two = tk.Label(frame1, text = 'Step 2:', font = 'Georgia 14', bg = '#f5e2bc')
step_two.grid(row = 2, column = 0)

b_setup = tk.Button(frame1, text = 'Setup', width = 10, bg='red', font = 'Arial 16', command = setup)
b_setup.grid(row = 2, column = 1)

step_three = tk.Label(frame1, text = 'Step 3:', font = 'Georgia 14', bg = '#f5e2bc')
step_three.grid(row = 3, column = 0)

b_start = tk.Button(frame1, text = 'Start!', width = 10, bg='#fcae18', font = 'Arial 16', command = main)
b_start.grid(row = 3, column = 1)

frame1.grid(row = 3, column = 0)

load = ImageTk.PhotoImage(Image.open('logo_1.jpg'))
img = tk.Label(root, image = load, bg = '#f5e2bc', pady = 20)
img.grid(row = 0, column = 0)

root.mainloop()

import cv2
import numpy as np
from random import randint
from functools import partial
from math import sqrt
import time

t0 = time.time()
def create_centers(N_of_centers):
    center = []
    for i in range (N_of_centers):
        center.append([randint(0,255), randint(0,255), randint(0,255)])
    return center

def distance(x1, y1, z1, x2, y2, z2):
    d = ((x1-x2)**2+(y1-y2)**2+(z1-z2)**2)
    return d

img = cv2.imread('img.jpg')
width = np.size(img, 0)
height = np.size(img, 1)
N_of_centers = int(input())
center = create_centers(N_of_centers)  
gaus = cv2.GaussianBlur (img, (51,51), 0)

print(center)

owner = [[1]*width for _ in range(height)]
for_average =[[] for _ in range(N_of_centers) ]

for _ in range(3):    
    for y in range(height):
        for x in range(width):
            s = 450**2
            x1, y1, z1 = gaus[x,y]
            for i in range(N_of_centers):
                x2, y2, z2 = center[i]
                f = partial(distance, x1=x1, y1=y1, z1=z1, x2=x2, y2=y2, z2=z2)
                d = distance(x1, y1, z1, x2, y2, z2)
                if s>d:
                    s=d
                    cluster=i    
            owner[y][x] = cluster
            for_average[cluster].append(gaus[x,y])
    center = [[sum(lll[i] for lll in ll)//(len(ll)+1) for i in range(3)] for ll in for_average]
    print(center)

for y in range(height):
    for x in range(width):
        i = owner[y][x]
        gaus[x, y] = np.array([center[i][0], center[i][1], center[i][2]])

cv2.imshow("result", gaus)
cv2.waitKey(0)

print('total time:', time.time() - t0)
